<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitaed65576f599b65797a2d4c9c9583dfc
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Product_Project\\' => 16,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Product_Project\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitaed65576f599b65797a2d4c9c9583dfc::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitaed65576f599b65797a2d4c9c9583dfc::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
